﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiDemo.DbContexts;
using WebApiDemo.Entities;
using Microsoft.EntityFrameworkCore;
using AutoMapper.Configuration;
using AutoMapper;

namespace WebApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly EmployeeDbContext _employeeDbContext;

        private readonly IMapper _mapper;

        public DepartmentsController(EmployeeDbContext employeeDbContext, IMapper mapper)
        {
            _employeeDbContext = employeeDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<DepartmentsDto>> GetAllAsync()
        {
            var departments = await _employeeDbContext.Departments
                .Include(d => d.Employees)
                .ToListAsync();

            return _mapper.Map<IEnumerable<DepartmentsDto>>(departments);

            //return departments.Select(d => new DepartmentsDto
            //{
            //    Id = d.Id,
            //    Name = d.Name,
            //    Employees = d.Employees?.Select(e => new EmployeeDto
            //    {
            //        Id = e.Id,
            //        Name = e.Name,
            //        Surname = e.Surname,
            //        Salary = e.Salary
            //    }) ?? Enumerable.Empty<EmployeeDto>()
            //});
        }

        [HttpGet("GetAverageSalaryForEachDepartment")]
        public async Task<IEnumerable<AvergeSalaryForEachDepartmentDto>> GetAverageSalaryForEachDepartment()
        {
            var items = from d in _employeeDbContext.Departments
                        join e in _employeeDbContext.Employees on d.Id equals e.DepartmentId into employeeTmp
                        from empl in employeeTmp.DefaultIfEmpty()
                        group empl by d.Name into g
                        select 
                        new AvergeSalaryForEachDepartmentDto(
                            g.Key, 
                            g.Average(r => r.Salary));

            return await items.ToListAsync();
        }
    }
}
