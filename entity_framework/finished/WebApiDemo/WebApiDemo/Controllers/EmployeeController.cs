﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiDemo.DbContexts;
using WebApiDemo.Entities;
using Microsoft.EntityFrameworkCore;

namespace WebApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeDbContext _employeeDbContext;

        private readonly IMapper _mapper;

        public EmployeeController(EmployeeDbContext employeeDbContext, IMapper mapper)
        {
            _employeeDbContext = employeeDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<EmployeeDto>> GetAllAsync()
        {
            var employee = await _employeeDbContext.Employees.ToListAsync();

            return _mapper.Map<IEnumerable<EmployeeDto>>(employee);
        }
    }
}
