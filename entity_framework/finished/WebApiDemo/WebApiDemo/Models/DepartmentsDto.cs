﻿namespace WebApiDemo.Entities
{
    public class DepartmentsDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public IEnumerable<EmployeeDto> Employees { get; set; }
    }
}