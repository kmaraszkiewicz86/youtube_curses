﻿namespace WebApiDemo.Entities
{
    public class AvergeSalaryForEachDepartmentDto
    {
        public AvergeSalaryForEachDepartmentDto(string name, decimal? sumary)
        {
            Name = name;
            Sumary = sumary.HasValue ? sumary.Value : 0;
        }

        public string Name { get; }

        public decimal Sumary { get; }
    }
}