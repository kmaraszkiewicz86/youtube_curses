﻿using Microsoft.EntityFrameworkCore;
using WebApiDemo.Entities;

namespace WebApiDemo.DbContexts
{
    public static class InitailDbData
    {
        private static readonly Departments[] departments = new Departments[]
        {
            new Departments { Id = 1, Name = "IT" },
            new Departments { Id = 2, Name = "Rada Nadzorcza" },
            new Departments { Id = 3, Name = "Produkcja" },
            new Departments { Id = 4, Name = "Dział Kadr" },
        };

        private static readonly Employee[] employees = new Employee[]
        {
            new Employee { Id = 1, Name = "Piotr", Surname = "M.", DepartmentId = 1, Salary = 21000 },
            new Employee { Id = 2, Name = "Szymon", Surname = "B.", DepartmentId = 1, Salary = 15000 },
            new Employee { Id = 3, Name = "Krzysztof", Surname = "S.", DepartmentId = 1, Salary = 18000 },
            new Employee { Id = 4, Name = "Daniel", Surname = "K.", DepartmentId = 2, Salary = 150000 },
            new Employee { Id = 5, Name = "Adrian", Surname = "K.", DepartmentId = 3, Salary = 6000 },
            new Employee { Id = 6, Name = "Mateusz", Surname = "W.", DepartmentId = 3, Salary = 3500 },
        };

        public static void InitializeDbData(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Departments>().HasData(departments);
            modelBuilder.Entity<Employee>().HasData(employees);
        }
    }
}
