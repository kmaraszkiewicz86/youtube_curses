﻿using Microsoft.EntityFrameworkCore;
using WebApiDemo.Entities;

namespace WebApiDemo.DbContexts
{
    public class EmployeeDbContext : DbContext
    {
        public DbSet<Departments> Departments { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public EmployeeDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Departments>().ToTable("Department");
            modelBuilder.Entity<Departments>()
                .Property(d => d.Name).IsRequired().HasMaxLength(50);

            modelBuilder.Entity<Employee>().ToTable("Employee")
                .Property(d => d.Name).IsRequired();
            modelBuilder.Entity<Employee>().ToTable("Employee")
                .Property(d => d.Surname).IsRequired();
            modelBuilder.Entity<Employee>().ToTable("Employee")
                .Property(d => d.Salary).HasConversion<double>().IsRequired();

            modelBuilder.Entity<Employee>().ToTable("Employee")
                .Property(d => d.DepartmentId).IsRequired();

            modelBuilder.Entity<Employee>().ToTable("Employee")
                .HasIndex(d => new { d.Name, d.Surname, d.DepartmentId }).IsUnique();

            modelBuilder.InitializeDbData();

            base.OnModelCreating(modelBuilder);
        }
    }
}
