﻿using System.Collections.ObjectModel;

namespace WebApiDemo.Entities
{
    public class Departments
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public Collection<Employee> Employees { get; set; }
    }
}