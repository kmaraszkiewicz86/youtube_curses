﻿namespace WebApiDemo.Entities
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public decimal Salary { get; set; }

        public int DepartmentId { get; set; }

        public Departments Department { get; set; }
    }
}
