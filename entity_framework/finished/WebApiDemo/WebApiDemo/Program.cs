using Microsoft.EntityFrameworkCore;
using WebApiDemo.DbContexts;
using AutoMapper;
using WebApiDemo.Entities;
using System.Diagnostics;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

var mapper = new MapperConfiguration(cfg =>
{
    cfg.CreateMap<Departments, DepartmentsDto>();
    cfg.CreateMap<Employee, EmployeeDto>();
}).CreateMapper();

builder.Services.AddSingleton<IMapper>(mapper);

builder.Services.AddDbContext<EmployeeDbContext>(opt =>
{
    opt.UseSqlite("Data Source=employee.db3");
    opt.LogTo(l => Debug.WriteLine(l));
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

//var dbContext = app.Services.GetRequiredService<EmployeeDbContext>();

using (var scoped = app.Services.CreateScope())
{
    var dbContext = scoped.ServiceProvider.GetRequiredService<EmployeeDbContext>();
    dbContext.Database.EnsureCreated();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
