using Microsoft.AspNetCore.Mvc;

namespace WebApiDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        public static IList<Person> _people { get; } = new List<Person>();

        public WeatherForecastController()
        {
        }

        [HttpGet()]
        public IEnumerable<Person> Get()
        {
            return _people;
        }

        [HttpPost]
        public ActionResult Add(Person person)
        {
            try
            {
                _people.Add(person);
                //return Ok(new Result { IsValid = true });
                return BadRequest(new Result { IsValid = true });
            }
            catch
            {
                return BadRequest(new Result { IsValid = true });
            }
        }
    }
}