﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppDemo
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainViewModel();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var name = Name.Text;

            if (string.IsNullOrWhiteSpace(name))
            {
                DisplayAlert("Błąd w formularzu", "Formularz zawiera puste pola", "OK");
                return;
            }

            DisplayAlert("Dane poprawne", name, "OK");
        }
    }
}
