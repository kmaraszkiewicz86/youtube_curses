﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppDemo
{
    internal class MainViewModel : INotifyPropertyChanged
    {
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
                OnClickCommand.ChangeCanExecute();
            }
        }

        private string _text;
        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        public ObservableCollection<Person> People { get; } = new ObservableCollection<Person>();

        public Command OnClickCommand { get; }

        public MainViewModel()
        {
            OnClickCommand = new Command(async () => await Button_ClickedAsync(), Validate);
        }

        private bool Validate() => !string.IsNullOrWhiteSpace(Name);

        private async Task Button_ClickedAsync()
        {
            Text = $"Dane zostały dodane poprawnie";

            try
            {
                await Add();
                await Fetch();
            }
            catch (Exception err)
            {
                Text = $"Wystąpił błąd podczas dodawnia nowego użytkownika...";
            }
        }

        private async Task Add()
        {
            HttpClient httpClient = new HttpClient();

            var person = new Person
            {
                Name = Name
            };

            var requestContent = new StringContent(JsonSerializer.Serialize(person),
                Encoding.UTF8,
                "application/json");

            var response = await httpClient.PostAsync("http://10.0.2.2:5193/WeatherForecast", requestContent);

            response.EnsureSuccessStatusCode();
        }

        private async Task Fetch()
        {
            HttpClient httpClient = new HttpClient();

            var response = await httpClient.GetAsync("http://10.0.2.2:5193/WeatherForecast");

            response.EnsureSuccessStatusCode();

            IEnumerable<Person> people = await JsonSerializer.DeserializeAsync<IEnumerable<Person>>(
                await response.Content.ReadAsStreamAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });

            People.Clear();
            foreach (Person personResult in people)
            {
                People.Add(personResult);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
