﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDemo1
{
    public class Person
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public bool Result { get; set; }
    }
}
