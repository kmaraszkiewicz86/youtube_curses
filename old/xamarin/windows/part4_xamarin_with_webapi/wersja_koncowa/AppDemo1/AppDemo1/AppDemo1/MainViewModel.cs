﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppDemo1
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;

                OnPropertyChanged(nameof(Name));
                ShowValuesCommand.ChangeCanExecute();
            }
        }

        private string _surname;

        public string Surname
        {
            get => _surname;
            set
            {
                _surname = value;

                OnPropertyChanged(nameof(Surname));
                ShowValuesCommand.ChangeCanExecute();
            }
        }

        private string _typedString;

        public string TypedString
        {
            get => _typedString;
            set
            {
                _typedString = value;

                OnPropertyChanged(nameof(TypedString));
            }
        }

        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();

        public Command ShowValuesCommand { get; }

        public MainViewModel()
        {
            ShowValuesCommand = new Command(async () =>
            {
                try
                {
                    await Button_ClickedAsync();
                }
                catch (Exception err)
                {
                    TypedString = "Został zgłoszony wyjątek";
                }

                TypedString = "Osoba została dodana do listy";
            }, Validate);
        }

        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(Surname);
        }

        private async Task Button_ClickedAsync()
        {
            await AddPersonAsync();
            await FetchData();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
            {
                if (cert.Issuer.Equals("CN=localhost"))
                    return true;
                return errors == System.Net.Security.SslPolicyErrors.None;
            };
            return handler;
        }

        private async Task<bool> AddPersonAsync()
        {
            var person = new Person
            {
                Name = Name,
                Surname = Surname
            };

            HttpClientHandler insecureHandler = GetInsecureHandler();

            using (var client = new HttpClient(insecureHandler))
            {
                var stringContent = new StringContent(JsonSerializer.Serialize(person),
                    Encoding.UTF8,
                    "application/json");

                var response = await client.PostAsync("https://10.0.2.2:7005/api/People", stringContent);

                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }

                var responeInString = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<Result>(responeInString);

                return result.IsValid;
            }
        }

        private async Task FetchData()
        {
            HttpClientHandler insecureHandler = GetInsecureHandler();

            using (var client = new HttpClient(insecureHandler))
            {
                var response = await client.GetAsync("https://10.0.2.2:7005/api/People");

                if (!response.IsSuccessStatusCode)
                {
                    return;
                }

                var people = await JsonSerializer.DeserializeAsync<IEnumerable<Person>>(
                    await response.Content.ReadAsStreamAsync(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });

                if (people != null)
                {
                    await Device.InvokeOnMainThreadAsync(() =>
                    {
                        foreach (Person person in people)
                        {
                            People.Add(person);
                        }
                    });
                }

            }
        }
    }
}
