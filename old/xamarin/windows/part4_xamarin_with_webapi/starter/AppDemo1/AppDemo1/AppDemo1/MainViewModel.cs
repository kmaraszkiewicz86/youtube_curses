﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace AppDemo1
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;

                OnPropertyChanged(nameof(Name));
                ShowValuesCommand.ChangeCanExecute();
            }
        }

        private string _surname;

        public string Surname
        {
            get => _surname;
            set
            {
                _surname = value;

                OnPropertyChanged(nameof(Surname));
                ShowValuesCommand.ChangeCanExecute();
            }
        }

        private string _typedString;

        public string TypedString
        {
            get => _typedString;
            set
            {
                _typedString = value;

                OnPropertyChanged(nameof(TypedString));
            }
        }

        public Command ShowValuesCommand { get; }

        public MainViewModel()
        {
            ShowValuesCommand = new Command(Button_Clicked, Validate);
        }

        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(Surname);
        }

        private void Button_Clicked()
        {
            TypedString = $"{Name} {Surname}";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
