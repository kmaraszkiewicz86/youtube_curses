﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppDemo1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            BindingContext = new MainViewModel();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var name = Name.Text;
            var surname = Surname.Text;

            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(surname))
            {
                DisplayAlert("Wymagana uwaga", "Formularz posiada błędy", "OK");
                return;
            }

            DisplayAlert("Wpisane dane", $"{name} {surname}", "OK");
        }
    }
}
