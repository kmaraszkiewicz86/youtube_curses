﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiDemo.Models;

namespace WebApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private static List<Person> _people { get; } = new List<Person>();

        [HttpGet]
        public List<Person> GetAll()
        {
            return _people;
        }

        [HttpPost]
        public ActionResult Add(Person person)
        {
            try
            {
                _people.Add(person);
            }
            catch (Exception)
            {
                return BadRequest(new { Result = false });
            }

            return Ok(new { Result = true });
        }
    }
}
