﻿namespace WebApiDemo.Models
{
    public class Person
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public bool Result { get; set; }
    }
}
